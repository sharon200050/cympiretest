# Field Validators - should check 
# Specified fields according to their name
def isValidStoreId(object):
    return True


def isValidCategoryId(object):
    return True


def isValidItemId(object):
    return True


def isValidPrice(object):
    return True


# Should check if object(which should be dictionary) contains
# All the relvent fields(store, category, etc)
# And whether isValidCategoryId(object.get("category")) && ...
# And then return True, otherwise false
def isValidItemDescriptor(object):
    return True


# Should check if object is a list
# And for each item isValidItemDescriptor is satisified
def isValidInventory(object):
    return True