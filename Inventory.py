import itertools
from numpy import median

class Inventory(object):
    """
    Inventory Management system 
    """

    def __init__(self, inventory_list):
        self.inventory_list = inventory_list

    def get_categories_for_stroe(self, store):
        """
        Given a store id you should return all the categories ids in the inventory.
        :param store: store id
        :return: all the categories ids in the inventory
        """
        inventory_list = self.inventory_list

        matching_items = filter(
            lambda item_descriptor: item_descriptor["store"] == store,
            inventory_list)

        return map(lambda item_descriptor: item_descriptor["category"],
                   matching_items)

    def get_item_inventory(self, item):
        """
        Given items name return all the items across all stores.
        :param item: item name
        :return: all the items across all stores
        """
        inventory_list = self.inventory_list

        matching_items = filter(
            lambda item_descriptor: item_descriptor["item_name"] == item,
            inventory_list)

        return matching_items

    def get_median_for_category(self, category):
        """
        Given category id return the median of the prices for all items in the category.
        :param category: category name
        :return: the median of the prices for all items in the category
        """
        inventory_list = self.inventory_list

        matching_items = filter(
            lambda item_descriptor: item_descriptor["category"] == category,
            inventory_list)

        # Item price is the price of a single item
        # Median should be the median of all prices of all items
        # For instance, for two shirts of price 1 and two pcs with
        # Of price two we should return the median of [1,1,2,2]
        replicated_prices = map(
                lambda item_descriptor: item_descriptor["items"] * [item_descriptor["price"]],
                matching_items)
        merged = list(itertools.chain.from_iterable(replicated_prices))
        if(len(merged) == 0):
            return 0
        return median(merged)
