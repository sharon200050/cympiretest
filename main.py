import json
from Inventory import Inventory
from flask import Flask, jsonify, request
app = Flask(__name__)

inventory_path = 'inventory.txt'
# TODO: add json validation
inventory_list = json.loads(open(inventory_path).read())
inventory = Inventory(inventory_list)


def asResultObject(result):
    return {"result": result}


@app.route("/inventory/categories", methods=['GET'])
def get_categories():
    store_id = request.args.get("store_id", type=int)
    return jsonify(
        asResultObject(inventory.get_categories_for_stroe(store_id)))


@app.route("/inventory/items", methods=['GET'])
def get_itmes():
    item_id = request.args.get("item_id", type=str)
    return jsonify(asResultObject(inventory.get_item_inventory(item_id)))


@app.route("/inventory/categories/median", methods=['GET'])
def get_median():
    category_id = request.args.get("category_id", type=int)
    return jsonify(asResultObject(inventory.get_median_for_category(category_id)))


app.run(port=5000, debug=True)