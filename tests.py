import unittest
from Inventory import Inventory
from numpy.testing import assert_almost_equal


class TestInventoryMethods(unittest.TestCase):
    def test_get_categories_for_stroe(self):
        assert (Inventory([]).get_categories_for_stroe(1) == [])
        assert (Inventory([{
            "store": 1,
            "category": 10,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        }]).get_categories_for_stroe(2) == [])
        assert (Inventory([{
            "store": 1,
            "category": 10,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        }]).get_categories_for_stroe(1) == [10])
        assert (Inventory([{
            "store": 1,
            "category": 10,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        },
        {
            "store": 2,
            "category": 1,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        },
        {
            "store": 2,
            "category": 2,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        }]).get_categories_for_stroe(2) == [1, 2])

    def test_get_item_inventory(self):
        assert (Inventory([]).get_item_inventory("some name") == [])

        assert (Inventory([{
            "store": 1,
            "category": 10,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        }]).get_item_inventory("pc") == [])

        assert (Inventory([{
            "store": 1,
            "category": 10,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        }]).get_item_inventory("shirt") == [{
            "store": 1,
            "category": 10,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        }])

        assert (Inventory([{
            "store": 1,
            "category": 10,
            "item_name": "shirt",
            "items": 10,
            "price": 100
        },
                           {
                               "store": 2,
                               "category": 10,
                               "item_name": "shirt",
                               "items": 10,
                               "price": 100
                           }]).get_item_inventory("shirt") == [{
                               "store":
                               1,
                               "category":
                               10,
                               "item_name":
                               "shirt",
                               "items":
                               10,
                               "price":
                               100
                           },
                                                               {
                                                                   "store":
                                                                   2,
                                                                   "category":
                                                                   10,
                                                                   "item_name":
                                                                   "shirt",
                                                                   "items":
                                                                   10,
                                                                   "price":
                                                                   100
                                                               }])

        assert (Inventory([{
            "store": 1,
            "category": 9,
            "item_name": "pc",
            "items": 10,
            "price": 100
        },
                           {
                               "store": 2,
                               "category": 10,
                               "item_name": "shirt",
                               "items": 10,
                               "price": 100
                           }]).get_item_inventory("shirt") == [{
                               "store":
                               2,
                               "category":
                               10,
                               "item_name":
                               "shirt",
                               "items":
                               10,
                               "price":
                               100
                           }])

    def test_get_median_for_category(self):
        assert_almost_equal(Inventory([]).get_median_for_category(1), 0.0)

        assert_almost_equal(
            Inventory([{
                "store": 1,
                "category": 10,
                "item_name": "shirt",
                "items": 1,
                "price": 100
            }]).get_median_for_category(10), 100.0)

        assert_almost_equal(
            Inventory([{
                "store": 1,
                "category": 10,
                "item_name": "shirt",
                "items": 50,
                "price": 100
            }]).get_median_for_category(10), 100.0)

        assert_almost_equal(
            Inventory([{
                "store": 1,
                "category": 10,
                "item_name": "shirt",
                "items": 50,
                "price": 100
            },
                       {
                           "store": 1,
                           "category": 11,
                           "item_name": "pc",
                           "items": 50,
                           "price": 100
                       }]).get_median_for_category(10), 100.0)

        assert_almost_equal(
            Inventory([{
                "store": 1,
                "category": 10,
                "item_name": "shirt",
                "items": 2,
                "price": 2
            },
                       {
                           "store": 2,
                           "category": 10,
                           "item_name": "shirt",
                           "items": 2,
                           "price": 3
                       }]).get_median_for_category(10), 2.5)

        assert_almost_equal(
            Inventory([{
                "store": 1,
                "category": 10,
                "item_name": "shirt",
                "items": 2,
                "price": 2
            },
                       {
                           "store": 2,
                           "category": 10,
                           "item_name": "pc",
                           "items": 2,
                           "price": 3
                       }]).get_median_for_category(10), 2.5)

if __name__ == '__main__':
    unittest.main()